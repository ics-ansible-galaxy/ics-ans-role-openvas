import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_openvas_config(host):
    conf = host.file("/etc/openvas/openvassd.conf")
    log = host.file("/etc/openvas/openvassd_log.conf")
    assert conf.is_file
    assert log.is_file


def test_openvas_service(host):
    scanner = host.service("openvas-scanner")
    assert scanner.is_running


def test_openvas_port(host):
    assert host.socket("tcp://0.0.0.0:443").is_listening
