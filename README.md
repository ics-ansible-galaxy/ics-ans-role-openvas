# ics-ans-role-openvas

Ansible role to install openvas.

## Requirements

- ansible >= 2.7
- molecule >= 2.19

## Role Variables

```yaml
openvas_administrator_username: admin
openvas_administrator_password: password # Remember to vault this
openvas_release_version: 1.0-21
...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-openvas
```

## License

BSD 2-clause
